using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class JSONSaving : MonoBehaviour
{
	#region Singleton 
	public static JSONSaving Instance { get; private set; }

	private void Awake()
	{

		if (Instance != null && Instance != this)
		{
			Destroy(this);
		}
		else
		{
			Instance = this;
		}
	}
	#endregion

	[SerializeField] private Button _startStopRecordButton;
	[SerializeField] private InputField _inputField;
	[SerializeField] private Button _loadButton;

	private string persistentPath = "";

	private void Start()
	{
		persistentPath = Application.persistentDataPath + Path.AltDirectorySeparatorChar + "SaveData.json";
		_startStopRecordButton.onClick.AddListener(StartStopRecording);
		_loadButton.onClick.AddListener(LoadRecording);
	}

	public void SaveData(string name, Dictionary<float, ReplayableObjectsActions> dto)
	{
		if (dto.Count == 0)
		{
			return;
		}
		string json = JsonUtility.ToJson(dto);
		StreamWriter writer = new StreamWriter(persistentPath + name);
		writer.Write(json);
	}

	public Dictionary<float, ReplayableObjectsActions> LoadData(string name)
	{
		StreamReader reader = new StreamReader(persistentPath + name);
		string json = reader.ReadToEnd();
		var dto = JsonUtility.FromJson<Dictionary<float, ReplayableObjectsActions>>(json);
		Debug.Log(dto);
		return dto;
	}

	private void StartStopRecording()
	{

	}

	private void LoadRecording()
	{

	}
}
