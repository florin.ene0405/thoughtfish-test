using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupController : ReplayableObjects
{
	[SerializeField] private Button _closeButton;

	private void Start()
	{
		_closeButton.onClick.AddListener(ClosePopup);
	}

	private void ClosePopup()
	{
		gameObject.SetActive(false);

		if (ManagerController.Instance.IsRecording())
		{
			ReplayableObjectsActions action = new ReplayableObjectsActions();
			action.Action = ClosePopup;
			AddAction(action);
		}
	}
}
