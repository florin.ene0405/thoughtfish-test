using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InteractableObjects : ReplayableObjects, IDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	[SerializeField] private int _startColorIndex;
	[SerializeField] private Canvas _movingArea;
	[SerializeField] private GameObject _tooltip;
	[SerializeField] private PopupController _popup;

	private RectTransform rectTransform;
	private Image _image;
	private int _colorIndex;
	private float _hoverTimeNeeded = 0.5f;
	private float _hoverTime;
	private bool _isHovering = false;
	private bool _isDragging = false;

	protected override void Start()
	{
		base.Start();
		rectTransform = GetComponent<RectTransform>();
		_image = GetComponent<Image>();
		_colorIndex = _startColorIndex;
	}

	protected override void Update()
	{
		base.Update();

		if (_isHovering && !_isDragging)
		{
			_hoverTime += Time.deltaTime;
			if (_hoverTime >= _hoverTimeNeeded)
			{
				ShowTooltip();
				_isHovering = false;
				_hoverTime = 0;

				if (ManagerController.Instance.IsRecording())
				{
					ReplayableObjectsActions action = new ReplayableObjectsActions();
					action.Action = ShowTooltip;
					AddAction(action);
				}
			}
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		_isDragging = true;
		rectTransform.anchoredPosition += eventData.delta / _movingArea.scaleFactor;

		if (ManagerController.Instance.IsRecording())
		{
			ReplayableObjectsActions action = new ReplayableObjectsActions
			{
				Position = rectTransform.anchoredPosition
			};
			AddAction(action);
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (_popup == null)
		{
			Debug.LogError("Popup null in " + gameObject.name);
			return;
		}

		if (_isDragging == true)
		{
			return;
		}

		ReplayableObjectsActions action = new ReplayableObjectsActions();

		if (eventData.button == PointerEventData.InputButton.Left)
		{
			ShowPopup();
			if (ManagerController.Instance.IsRecording())
			{
				action.Action = ShowPopup;
				AddAction(action);
			}
		}

		else if (eventData.button == PointerEventData.InputButton.Right)
		{
			_colorIndex++;

			if (_colorIndex >= ManagerController.Instance.Colors.Count)
			{
				_colorIndex = 0;
			}
			_image.color = ManagerController.Instance.Colors[_colorIndex];

			if (ManagerController.Instance.IsRecording())
			{
				action.Color = ManagerController.Instance.Colors[_colorIndex];
				AddAction(action);
			}
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		_isHovering = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_isHovering = false;
		_isDragging = false;
		HideToolTip();

		if (ManagerController.Instance.IsRecording())
		{
			ReplayableObjectsActions action = new ReplayableObjectsActions();
			action.Action = HideToolTip;
			AddAction(action);
		}
	}

	private void ShowPopup()
	{
		_popup.gameObject.SetActive(true);
	}

	private void ShowTooltip()
	{
		if (_tooltip == null)
		{
			LogTooltipError();
			return;
		}
		_tooltip.gameObject.SetActive(true);
	}

	private void HideToolTip()
	{
		if (_tooltip == null)
		{
			LogTooltipError();
			return;
		}
		_tooltip.gameObject.SetActive(false);
	}

	private void LogTooltipError()
	{
		Debug.LogError("Tooltip null in " + gameObject.name);
	}
}
