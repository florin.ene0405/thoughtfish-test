using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ReplayableObjectsActions
{
	public UnityAction Action;
	public Vector2 Position;
	public Color32 Color;
}
