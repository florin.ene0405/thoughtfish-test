using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplayableObjects : MonoBehaviour
{
	private Dictionary<float, ReplayableObjectsActions> _replayableObjectsActions = new Dictionary<float, ReplayableObjectsActions>();
	private float _recordTimer;


	protected virtual void Start()
	{
		ManagerController.Instance.StartRecording += StartRecording;
		ManagerController.Instance.StopRecording += StopRecording;
		ManagerController.Instance.LoadRecording += LoadRecording;
	}

	protected virtual void Update()
	{
		_recordTimer += Time.deltaTime;
		if (Input.GetKeyDown(KeyCode.A))
		{
			StopRecording();
		}
		
		if (Input.GetKeyDown(KeyCode.S))
		{
			LoadRecording();
		}
	}

	public void AddAction(ReplayableObjectsActions action)
	{
		_replayableObjectsActions.Add(_recordTimer, action);
	}

	private void StartRecording()
	{
		_recordTimer = 0;
	}

	private void StopRecording()
	{
		JSONSaving.Instance.SaveData(gameObject.name, _replayableObjectsActions);
	}

	private void LoadRecording()
	{
		_replayableObjectsActions.Clear();
		_replayableObjectsActions = JSONSaving.Instance.LoadData(gameObject.name);
	}
}
