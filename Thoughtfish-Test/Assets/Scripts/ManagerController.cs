using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerController : MonoBehaviour
{
	#region Singleton 
	public static ManagerController Instance { get; private set; }

	private void Awake()
	{

		if (Instance != null && Instance != this)
		{
			Destroy(this);
		}
		else
		{
			Instance = this;
		}
	}
	#endregion

	public List<Color32> Colors = new List<Color32>();
	private bool _isRecording = true;
	private string _recordingName;

	public Action StartRecording;
	public Action StopRecording;
	public Action LoadRecording;

	public string GetRecordingName() => _recordingName;
	public bool IsRecording() => _isRecording;

	private void Start()
	{
		StartRecording += () =>  _isRecording = true ;
		StopRecording += () =>  _isRecording = false ;
	}
}
